package com.online_store.factoryalertdialogapp

import android.app.AlertDialog
import android.content.Context

class FactoryAlertDialog {
    companion object{
        fun createAlertDialog(context: Context, code: CodeAlertDialog): AlertDialog{
            return AlertDialog.Builder(context)
                .setTitle(getTitle(code))
                .setMessage(getMessage(code))
                .setNegativeButton("OK", null)
                .create()
        }

        private fun getTitle(code: CodeAlertDialog): String{
            return when(code) {
                CodeAlertDialog.ERROR_AUTH -> "Ошибка авторизации"
                CodeAlertDialog.ERROR_REG -> "Ошибка регистрации"
                CodeAlertDialog.ERROR_FIELDS_AUTH -> "Ошибка заполения полей"
                CodeAlertDialog.ERROR_FIELDS_REG -> "Ошибка заполения полей"
            }
        }

        private fun getMessage(code: CodeAlertDialog): String{
            return when(code){
                CodeAlertDialog.ERROR_AUTH -> "Повторите попытку позже"
                CodeAlertDialog.ERROR_REG -> "Повторите попытку позже"
                CodeAlertDialog.ERROR_FIELDS_AUTH -> "Заполните обязательнае поля: Почта и Пароль"
                CodeAlertDialog.ERROR_FIELDS_REG -> "Заполните обязательнае поля: Имя, Фамилия, Почта и Пароль"
            }
        }
    }
}

enum class CodeAlertDialog(val indetofivator: Int) {
    ERROR_AUTH(0), ERROR_REG(1), ERROR_FIELDS_AUTH(2), ERROR_FIELDS_REG(3)
}

