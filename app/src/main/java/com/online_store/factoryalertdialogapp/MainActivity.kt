package com.online_store.factoryalertdialogapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.IllegalArgumentException

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickToCreateAndShowAlertDialog(view: View){
        val code = when(view) {
            auth -> CodeAlertDialog.ERROR_AUTH
            reg -> CodeAlertDialog.ERROR_REG
            fileds_auth -> CodeAlertDialog.ERROR_FIELDS_AUTH
            fileds_reg -> CodeAlertDialog.ERROR_FIELDS_REG
            else -> throw IllegalArgumentException("AlertDialogCode for this view not found")
        }

        val dialog = FactoryAlertDialog.createAlertDialog(this, code)
        dialog.show()
    }
}